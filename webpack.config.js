var path = require('path');
var webpack = require('webpack');
 module.exports = {
     entry: './src/index.jsx',
     output: {
         path: path.resolve(__dirname, 'build'),
         filename: 'app.bundle.js'
     },
     watch: true,
     module: {
        rules: [
            {
              test: /\.(js|jsx)$/,
              exclude: /node_modules/,
              loader: 'babel-loader',
              query:
              {
                presets:['react']
              }
            },
            {
              test: /\.css$/, 
              loader: 'style-loader'
            },
            {
              test: /\.css$/, 
              loader: 'css-loader',
              query: {
                modules: true,
                localIdentName: '[name]__[local]___[hash:base64:5]'
              }
            }
          ]
     },
     stats: {
         colors: true
     },
     devtool: 'source-map'
 };