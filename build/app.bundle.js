/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.jsx");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/index.jsx":
/*!***********************!*\
  !*** ./src/index.jsx ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

const cardContainer = document.querySelector(".react-card");

// React component for form inputs
class CardInput extends React.Component {
  render() {
    return React.createElement(
      "fieldset",
      null,
      React.createElement("input", {
        name: this.props.name,
        id: this.props.id,
        type: this.props.type || "text",
        placeholder: this.props.placeholder,
        required: true
      })
    );
  }
}

// React component for textarea
class CardTextarea extends React.Component {
  render() {
    return React.createElement(
      "fieldset",
      null,
      React.createElement("textarea", {
        name: this.props.name,
        id: this.props.id,
        placeholder: this.props.placeholder,
        required: true
      })
    );
  }
}

// React component for form button
class CardBtn extends React.Component {
  render() {
    return React.createElement(
      "fieldset",
      null,
      React.createElement(
        "button",
        {
          className: this.props.className,
          type: this.props.type,
          value: this.props.value
        },
        this.props.value
      )
    );
  }
}

// React component for social profile links
class CardProfileLinks extends React.Component {
  render() {
    const profileLinks = ["twitter", "linkedin", "gitlab", "medium"];
    const mediaLinks = ["https://twitter.com/RozaJayBird", "https://www.linkedin.com/in/roza-jiang-5776b55a/", "https://gitlab.com/rozajay", "https://medium.com/@rozajiang"];
    const linksList = profileLinks.map((link, index) => React.createElement(
      "li",
      { key: index },
      React.createElement(
        "a",
        { href: mediaLinks[index], target: "_blank" },
        React.createElement("i", { className: "fa fa-" + link })
      )
    ));

    return React.createElement(
      "div",
      { className: "card-social-links" },
      React.createElement(
        "ul",
        { className: "social-links" },
        linksList
      )
    );
  }
}

// React component for the front side of the card
class CardFront extends React.Component {
  render() {
    return React.createElement(
      "div",
      { className: "card-side side-front" },
      React.createElement(
        "div",
        { className: "container-fluid" },
        React.createElement(
          "div",
          { className: "row" },
          React.createElement(
            "div",
            { className: "col-xs-6" },
            React.createElement("img", { src: "media/GetPersonaPhoto.jpg" })
          ),
          React.createElement(
            "div",
            { className: "col-xs-6 side-front-content" },
            React.createElement(
              "h2",
              null,
              "Melbourne based"
            ),
            React.createElement(
              "h1",
              null,
              "Software Developer"
            ),
            React.createElement(
              "p",
              null,
              "Roza is driven by problem solving and putting fun software concepts into practice."
            ),
            React.createElement(
              "p",
              null,
              "She is currently working as a software graduate at Telstra."
            )
          )
        )
      )
    );
  }
}

// React component for the back side of the card
class CardBack extends React.Component {
  render() {
    return React.createElement(
      "div",
      { className: "card-side side-back" },
      React.createElement(
        "div",
        { className: "container-fluid" },
        React.createElement(
          "h1",
          null,
          "Let's get in touch!"
        ),
        React.createElement(
          "form",
          { formAction: "", className: "card-form" },
          React.createElement(
            "div",
            { className: "row" },
            React.createElement(
              "div",
              { className: "col-xs-6" },
              React.createElement(CardInput, {
                name: "contactFirstName",
                id: "contactFirstName",
                type: "text",
                placeholder: "Your first name"
              })
            ),
            React.createElement(
              "div",
              { className: "col-xs-6" },
              React.createElement(CardInput, {
                name: "contactLastName",
                id: "contactLastName",
                type: "text",
                placeholder: "Your last name"
              })
            )
          ),
          React.createElement(
            "div",
            { className: "row" },
            React.createElement(
              "div",
              { className: "col-xs-6" },
              React.createElement(CardInput, {
                name: "contactEmail",
                id: "contactEmail",
                type: "email",
                placeholder: "Your email address"
              })
            ),
            React.createElement(
              "div",
              { className: "col-xs-6" },
              React.createElement(CardInput, {
                name: "contactSubject",
                id: "contactSubject",
                type: "text",
                placeholder: "Subject"
              })
            )
          ),
          React.createElement(CardTextarea, {
            name: "contactMessage",
            id: "contactMessage",
            placeholder: "Your message"
          }),
          React.createElement(CardBtn, {
            className: "btn btn-primary",
            type: "submit",
            value: "Send message"
          })
        ),
        React.createElement(CardProfileLinks, null)
      )
    );
  }
}

// React component for the card (main component)
class Card extends React.Component {
  render() {
    return React.createElement(
      "div",
      { className: "card-container" },
      React.createElement(
        "div",
        { className: "card-body" },
        React.createElement(CardBack, null),
        React.createElement(CardFront, null)
      )
    );
  }
}

// Render Card component
ReactDOM.render(React.createElement(Card, null), cardContainer);

/***/ })

/******/ });
//# sourceMappingURL=app.bundle.js.map